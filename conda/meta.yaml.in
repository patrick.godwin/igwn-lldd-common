{% set name = "igwn-lldd-common" %}
{% set version = "0.0.0" %}

package:
  name: {{ name }}
  version: {{ version }}

source:
  url: @TARBALL@
  sha256: @SHA256@


build:
  entry_points:
    - kafka2frame = igwn_lldd_common.kafka2frame:main
    - frame2kafka = igwn_lldd_common.frame2kafka:main
  noarch: python
  number: 0
  script: {{ PYTHON }} -m pip install . -vvv

requirements:
  host:
    - pip
    - python >=3.6
    - setuptools >=38.2.5
  run:
    - configargparse
    - gpstime
    - python >=3.6, <3.12  # kafka-python doesn't yet support python 3.12
    - python-confluent-kafka
    - python-framel >=8.47.2
    - watchdog

test:
  requires:
    - kafka-python
    - pip
    - pytest
    - pytest-cov
    - pytest-console-scripts
    - pytest-rerunfailures
  commands:
    # check metadata
    - python -m pip check igwn-lldd-common
    - python -m pip show igwn-lldd-common
    # run test suite
    - python -m pytest --cov=igwn_lldd_common -ra -v --pyargs igwn_lldd_common.tests
    # print out the coverage
    - python -m coverage report
    # put coverage in a xml file
    - python -m coverage xml -o coverage-conda.xml

about:
  home: "https://git.ligo.org/computing/lowlatency/igwn-lldd-common"
  license: "GPL-3.0-or-later"
  license_family: "GPL"
  license_file: "LICENSE"
  summary:
    The IGWN - Low Latency Data Distribution (lldd) is software
    to distribute low latency data used by the International
    Gravitational-Wave Observatory Network (IGWN).

extra:
  recipe-maintainers:
    - rhyspoulton
