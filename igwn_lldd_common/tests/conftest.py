import logging


logging.getLogger("kafka").setLevel(logging.WARNING)
logging.getLogger("watchdog").setLevel(logging.WARNING)
